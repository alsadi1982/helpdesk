package ru.edu.helpdesk.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.edu.helpdesk.entity.*;
import ru.edu.helpdesk.repository.TicketRepository;
import ru.edu.helpdesk.repository.TitleRepository;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * TicketServiceImpl Test
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class TicketServiceImplTest {

    @Autowired
    private TicketService ticketService;

    @MockBean
    private TicketRepository ticketRepository;

    @MockBean
    private TitleRepository titleRepository;

    private final User SUPPORT = new User(1L, "support", "support", "support", "support", UserRole.SUPPORT);
    private final User USER1 = new User(2L, "user1", "user1", "user1", "user1", UserRole.USER);
    private final Title NETWORK = new Title(1L, "Проблема с NETWORK", false);
    private final Ticket TICKET1 = new Ticket(1L, USER1, SUPPORT, NETWORK, "Проблема с NETWORK", TicketStatus.OPEN, LocalDateTime.now());
    private final List<Ticket> TICKETS = Collections.singletonList(TICKET1);
    private final List<Title> TITLES = Collections.singletonList(NETWORK);


    @Test
    public void createTicket() {
        Ticket ticket = new Ticket();
        Assertions.assertDoesNotThrow(() -> ticketService.createTicket(ticket));
        Assertions.assertNotNull(ticket.getStatus());
        Assertions.assertNotNull(ticket.getCreationAt());

        Mockito.verify(ticketRepository, Mockito.times(1)).save(ticket);
    }

    @Test
    void ticketInfo() {
        Mockito.when(ticketRepository.getById(TICKET1.getId())).thenReturn(TICKET1);

        Assertions.assertDoesNotThrow(() -> ticketService.ticketInfo(TICKET1.getId()));
        Assertions.assertNull(ticketService.ticketInfo(100L));
        Assertions.assertNotNull(ticketService.ticketInfo(TICKET1.getId()));
        Assertions.assertEquals(USER1, ticketService.ticketInfo(TICKET1.getId()).getClient());
        Assertions.assertEquals(SUPPORT, ticketService.ticketInfo(TICKET1.getId()).getSupport());
        Assertions.assertEquals(NETWORK, ticketService.ticketInfo(TICKET1.getId()).getTitle());
        Assertions.assertEquals(TicketStatus.OPEN, ticketService.ticketInfo(TICKET1.getId()).getStatus());
    }

    @Test
    void allTicketsByClientId() {
        Mockito.when(ticketRepository.getAllByClient_Id(USER1.getId())).thenReturn(TICKETS);

        Assertions.assertDoesNotThrow(() -> ticketService.allTicketsByClientId(USER1.getId()));
        Assertions.assertTrue(ticketService.allTicketsByClientId(100L).isEmpty());
    }

    @Test
    void allTitles() {
        Mockito.when(titleRepository.findAllByMarkFalse()).thenReturn(TITLES);

        Assertions.assertDoesNotThrow(() -> ticketService.allTitles());
        Assertions.assertIterableEquals(TITLES, ticketService.allTitles());
    }
}