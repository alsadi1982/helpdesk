package ru.edu.helpdesk.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;


/**
 * Заявка на тех.поддержку
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "client_id")
    private User client;

    @ManyToOne
    @JoinColumn(name = "support_id")
    private User support;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Title title;

    @Column(nullable = false)
    @Size(min = 3, max = 256)
    private String description;

    @Enumerated(EnumType.ORDINAL)
    private TicketStatus status = TicketStatus.OPEN;

    @Column
    private LocalDateTime creationAt = LocalDateTime.now();

    public boolean isOpen() {
        return status == TicketStatus.OPEN;
    }

    public boolean isWorking() {
        return status == TicketStatus.WORKING;
    }

    public boolean isCompleted() {
        return status == TicketStatus.COMPLETED;
    }

    public boolean isRejected() {
        return status == TicketStatus.REJECTED;
    }

    public String getDisplayStatus() {
        if (status == TicketStatus.OPEN) {
            return "Открытый";
        }
        if (status == TicketStatus.REJECTED) {
            return "Отклонено";
        }
        if (status == TicketStatus.WORKING) {
            return "В работе";
        }
        if (status == TicketStatus.COMPLETED) {
            return "Закрыто";
        }
        return "Не определенно";
    }
}
